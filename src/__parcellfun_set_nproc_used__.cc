/*

Copyright (C) 2020 Olaf Till <i7tiol@t-online.de>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>.

*/

// PKG_ADD: autoload ("__parcellfun_set_nproc_used__", "__parcellfun_interface__.oct");
// PKG_DEL: autoload ("__parcellfun_set_nproc_used__", "__parcellfun_interface__.oct", "remove");

#include "parallel-gnutls.h"

namespace octave_parallel
{
  control &get_scheduler (void);
}

DEFUN_DLD (__parcellfun_set_nproc_used__, args, nargout,
           "-*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{nproc} =} __parcellfun_set_nproc_used__ (@var{nproc_arg})\n\
Undocumented internal function.\n\
\n\
@end deftypefn")
{
  std::string fname ("__parcellfun_set_nproc_used__");

  octave_value retval;

  if (args.length () != 1
      || ! args(0).OV_ISREAL ()
      || ! args(0).is_scalar_type ())
    {
      print_usage ();

      return retval;
    }

  octave_parallel::control &sched = octave_parallel::get_scheduler ();

  if (sched.set_nproc_used (args(0).int_value ())
      || ! sched.good ())
    {
      error ("%s: could not set number of processes", fname.c_str ());
    }
  else
    {
      retval = octave_value (sched.get_nproc_used ());
    }

  return retval;
}
