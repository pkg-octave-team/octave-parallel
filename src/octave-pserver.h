/*

Copyright (C) 2019, 2020 Olaf Till <i7tiol@t-online.de>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>.

*/

#include "config.h"

#if HAVE_UNISTD_H
#include <unistd.h>
#endif

#include <getopt.h>

#include "common.h"

#define N_LONGOPTS 7

class p_cmdline_options
{
public:

  p_cmdline_options (int argc, char * const *argv);

  bool use_tls (void) const { return opt_use_tls; }

  bool print_help (void) const { return opt_print_help; }

  const std::string &auth_file (void) const { return opt_auth_file; }

  const std::string &octave_binary (void) const { return opt_octave_binary; }

  int cmd_port (void) const { return opt_cmd_port; }

  int data_port (void) const { return opt_data_port; }

  const std::string &cmd_port_string (void) const
  { return opt_cmd_port_string; }

  const std::string &data_port_string (void) const
  { return opt_data_port_string; }

private:

  bool opt_use_tls;

  bool opt_print_help;

  std::string opt_auth_file;

  std::string opt_octave_binary;

  int opt_cmd_port;

  int opt_data_port;

  std::string opt_cmd_port_string;

  std::string opt_data_port_string;

  static
  struct option known_long_opts[N_LONGOPTS + 1];
};
