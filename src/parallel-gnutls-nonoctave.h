/*

Copyright (C) 2019 Olaf Till <i7tiol@t-online.de>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __OCT_PARALLEL_GNUTLS_NONOCTAVE__

#define __OCT_PARALLEL_GNUTLS_NONOCTAVE__

/* define some of the following for debugging, but take care:
   sensitive data (e.g. the password) will be written to standard
   error and to logfiles */
#undef octave_parallel_debug_server
// #define octave_parallel_debug_server 1
#undef octave_parallel_debug_client
// #define octave_parallel_debug_client 1
#undef octave_parallel_debug_lib
// #define octave_parallel_debug_lib 1

#ifdef octave_parallel_debug_server
  #define dsprintf(...) fprintf (stderr, __VA_ARGS__); fflush (stderr);
#else
  #define dsprintf(...)
#endif

#ifdef octave_parallel_debug_client
  #define dcprintf(...) fprintf (stderr, __VA_ARGS__);
#else
  #define dcprintf(...)
#endif

#ifdef octave_parallel_debug_lib
  #define dlprintf(...) fprintf (stderr, __VA_ARGS__); fflush (stderr);
#else
  #define dlprintf(...)
#endif

#endif // __OCT_PARALLEL_GNUTLS_NONOCTAVE__
