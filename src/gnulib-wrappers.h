/*

Copyright (C) 2019 Olaf Till <i7tiol@t-online.de>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __OCT_PARALLEL_GNULIB_WRAPPERS__

#define __OCT_PARALLEL_GNULIB_WRAPPERS__

#include "config.h"

#ifdef __cplusplus
extern "C" {
#endif

// only used for gnulib sockets
int gnulib_close (int fd);

// requires gnulib replacements for close (and ioctl, but the latter
// is not used in this package)
int gnulib_socket_pfinet_sockstream (int protocol);

int gnulib_connect (int sockfd);

ssize_t gnulib_recv (int sockfd, void *buf, size_t len, int flags);

ssize_t gnulib_send (int sockfd, const void *buf, size_t len, int flags);

void *gnulib_malloc (size_t size);

void gnulib_freeaddrinfo (void);

const char *gnulib_get_canonname (const char *hostname);

int gnulib_set_port (const char *hostname, const char *port);

void *gnulib_alloc_pollfds (int n);

int gnulib_poll_in_wrapper (int *in_out, void *pfds_arg, int nfds, int timeout);

int gnulib_pollin_1_noblock (int fd);

int gnulib_pollin_2_block (int fd1, int fd2, int *rev1, int *rev2);

int gnulib_get_fd_setsize (void);

void gnulib_fd_zero_all (void);

void gnulib_add_to_rfds (int fd);

void gnulib_add_to_wfds (int fd);

void gnulib_add_to_efds (int fd);

int gnulib_select (int nfds, double dtimeout, char **err_msg);

int gnulib_fd_isset_rfds (int fd);

int gnulib_fd_isset_wfds (int fd);

int gnulib_fd_isset_efds (int fd);

int gnulib_fstat_reg_exec_nonsuid (int fd);

void gnulib_perror (const char *s);

// 'sigset'-related stuff

void *gnulib_alloc_sigset (void);

int gnulib_get_sigmask (void *dest);

int gnulib_set_sigmask (void *src);

int gnulib_sigismember (void *sig_set, int signum);

void gnulib_set_newset_one_signal (void *sig_set, int signum);

int gnulib_unblock (void *sig_set);

// 'sigaction'-related stuff

void *gnulib_alloc_sigaction (void);

int gnulib_getsigaction (int signum, void *target);

int gnulib_setsigaction (int signum, void *src);

void gnulib_copy_sigaction (void *dest, const void *src);

void gnulib_remove_sa_restart (void *sig_action);

int gnulib_install_sighandler (int signum, void (*handler) (int));

// end of 'sigaction'-related stuff

uint32_t gnulib_htonl (uint32_t hostlong);

uint32_t gnulib_ntohl (uint32_t netlong);

int gnulib_mkdir_mode_0700 (const char *pathname);

char* gnulib_getpass (const char *prompt);

#ifdef __cplusplus
}
#endif

struct gnulib_guard
{
  void (*cleanup_function) (void);

  gnulib_guard (void (*cleanup_function_arg) (void))
  : cleanup_function (cleanup_function_arg)
  {

  }

  ~gnulib_guard (void)
  {
    cleanup_function ();
  }
};

struct gnulib_close_guard
{
  int fd;

  gnulib_close_guard (int fd_arg)
  : fd (fd_arg)
  {

  }

  ~gnulib_close_guard (void)
  {
    if (fd > -1)
      gnulib_close (fd);
  }

  void release (void)
  {
    fd = -1;
  }
};

#endif // __OCT_PARALLEL_GNULIB_WRAPPERS__
