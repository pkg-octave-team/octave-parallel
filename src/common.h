// This file is in the public domain.

// these ports were originally used by pserver
// ports 12346--12752 are unassigned by IANA as of 2019-04-20
#define DEFAULT_CMD_PORT "12502"
#define DEFAULT_DATA_PORT "12501"
