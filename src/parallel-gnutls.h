/*

Copyright (C) 2015-2023 Olaf Till <i7tiol@t-online.de>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __OCT_PARALLEL_GNUTLS__

#define __OCT_PARALLEL_GNUTLS__

// to have Octaves config.h included before config.h of 'parallel'
#include <octave/oct.h>

#include "config.h"

#ifdef HAVE_LIBGNUTLS
#include <gnutls/gnutls.h>
#endif

#include <sys/stat.h>

#include <sys/types.h>

#include <fcntl.h>

#include <errno.h>

#include <stdio.h>

#include <unistd.h>

#include <stdint.h>

// Octave includes

#include <octave/oct.h>

#include <octave/file-stat.h>

#include <octave/file-ops.h>

#include <octave/parse.h>

#include <octave/ov-struct.h>

#include <octave/version.h>


#define N_CONNECT_RETRIES 10

// parallel package includes

#include "parallel-gnutls-nonoctave.h"

#include "error-helpers.h"

#ifdef HAVE_LIBGNUTLS
#include "sensitive-data.h"
#endif

// This gnulib file currently doesn't seem dangerous for C++, it just
// defines an enum and the prototype of num_processors(...), which
// isn't a system function but specific to gnulib. Note that older
// versions of liboctave used to export the symbol of it's
// gnulib-provided num_processors(...) for linking, whereas newer
// versions don't do this and so our packages gnulib has to provide
// this function; FIXME: prevent linking against (older) liboctave's
// num_processors(...)?
#include "gl/nproc.h"

#include "gnulib-wrappers.h"

#include "p-pipes.h"

#include "p-streams.h"

#include "p-connection.h"

#include "p-sighandler.h"

#include "minimal-load-save.h"

#include "p-control.h"

#ifdef HAVE_LIBGNUTLS
void parallel_gnutls_set_mem_functions (void);
#endif

#endif // __OCT_PARALLEL_GNUTLS__
