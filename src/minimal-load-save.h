/*

Copyright (C) 2016-2018 Olaf Till <i7tiol@t-online.de

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program; if not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __OCT_PARALLEL_LOAD_SAVE__

#define __OCT_PARALLEL_LOAD_SAVE__

#include <octave/oct.h>

#include <octave/load-save.h>

#include "config.h"

int minimal_read_header (std::istream& is, bool& swap,
                         OCTAVE__MACH_INFO::float_format& flt_fmt);

int minimal_read_data (std::istream& is, octave_value& val,
                       bool swap = false,
                       OCTAVE__MACH_INFO::float_format flt_fmt
                       = OCTAVE__MACH_INFO::native_float_format ());

void minimal_write_header (std::ostream& os);

int minimal_write_data (std::ostream& os, octave_value& val);

#endif // __OCT_PARALLEL_LOAD_SAVE__
