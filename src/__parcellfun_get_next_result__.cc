/*

Copyright (C) 2020 Olaf Till <i7tiol@t-online.de>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>.

*/

// PKG_ADD: autoload ("__parcellfun_get_next_result__", "__parcellfun_interface__.oct");
// PKG_DEL: autoload ("__parcellfun_get_next_result__", "__parcellfun_interface__.oct", "remove");

#include "parallel-gnutls.h"

namespace octave_parallel
{
  control &get_scheduler (void);
}

DEFUN_DLD (__parcellfun_get_next_result__, args, nargout,
           "-*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{idx}, @var{result} =} __parcellfun_get_next_result__ ()\n\
Undocumented internal function.\n\
\n\
@end deftypefn")
{
  std::string fname ("__parcellfun_get_next_result__");

  if (args.length () != 0)
    {
      print_usage ();

      return octave_value ();
    }

  octave_parallel::control &sched = octave_parallel::get_scheduler ();

  octave_value idx;

  octave_value result;

  idx = sched.get_next_result (result);

  if (! result.is_defined () || ! sched.good ())
    {
      error ("%s: could not receive result", fname.c_str ());

      return octave_value ();
    }

  octave_value_list ret (2);

  ret(0) = idx;

  ret(1) = result;

  return ret;
}
