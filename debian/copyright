Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Parallel package for Octave
Upstream-Contact: Olaf Till <i7tiol@t-online.de>
Source: https://gnu-octave.github.io/packages/parallel/

Files: *
Copyright: 2007-2023 Olaf Till <i7tiol@t-online.de>
           2009-2010 VZLU Prague, a.s., Czech Republic
           2002 Hayato Fujiwara <h_fujiwara@users.sourceforge.net>
           2010 Jean-Benoist Leger <jben@jben.info>
           2009 Travis Collier <travcollier@gmail.com>
           2009 Jaroslav Hajek <highegg@gmail.com>
           2012-2014, 2018-2019 Rik Wehbring
License: GPL-3+

Files: octave-parallel.metainfo.xml
Copyright: 2016 Colin B. Macdonald
License: FSFAP
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved.  This file is offered as-is,
 without any warranty.

Files: inst/__internal_exit__.m
Copyright: none
License: public-domain
 This code is in the public domain.

Files: debian/*
Copyright: 2008 Ólafur Jens Sigurðsson <ojsbug@gmail.com>
           2008, 2009, 2012, 2016, 2018-2023 Rafael Laboissière <rafael@debian.org>
           2008-2011 Thomas Weber <tweber@debian.org>
           2014-2016, 2023 Sébastien Villemot <sebastien@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 3, can be found in the file
 `/usr/share/common-licenses/GPL-3'.
