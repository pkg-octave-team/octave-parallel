disp ("[parcellfun]");

### Assert result
try
    assert (parcellfun (1, @(x) x ^ 2, {1, 2, 3}),
            [1, 4, 9]);
    count = 1;
catch
    count = 0;
end_try_catch

disp (sprintf ("PASSES %d out of 1 test", count));
