<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- General documentation for the parallel package for Octave.

Copyright (C) 2016-2023 Olaf Till <i7tiol@t-online.de>

You can redistribute this documentation and/or modify it under the terms
of the GNU General Public License as published by the Free Software
Foundation; either version 3 of the License, or (at your option) any
later version.

This documentation is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this documentation; if not, see <http://www.gnu.org/licenses/>. -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>pserver (parallel_doc)</title>

<meta name="description" content="pserver (parallel_doc)">
<meta name="keywords" content="pserver (parallel_doc)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<link href="index.html" rel="start" title="Top">
<link href="Function-index.html" rel="index" title="Function index">
<link href="Cluster-execution.html" rel="up" title="Cluster execution">
<link href="pconnect.html" rel="next" title="pconnect">
<link href="Authentication.html" rel="prev" title="Authentication">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="pserver"></span><div class="header">
<p>
Next: <a href="pconnect.html" accesskey="n" rel="next">pconnect</a>, Previous: <a href="Authentication.html" accesskey="p" rel="prev">Authentication</a>, Up: <a href="Cluster-execution.html" accesskey="u" rel="up">Cluster execution</a> &nbsp; [<a href="Function-index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Starting-servers"></span><h3 class="section">3.3 Starting servers</h3>
<span id="index-pserver-3"></span>

<span id="XREFpserver"></span><dl>
<dt id="index-pserver">Function File: <em></em> <strong>pserver</strong> <em>()</em></dt>
<dt id="index-pserver-1">Function File: <em></em> <strong>pserver</strong> <em>(<var>options</var>)</em></dt>
<dt id="index-pserver-2">Function File: <em></em> <strong>pserver</strong> <em>(&quot;kill&quot;)</em></dt>
<dd><p>Starts or stops a server of the parallel cluster.
</p>
<p><var>options</var>: structure of options; field <code>use_tls</code> is
<code>true</code> by default (TLS with SRP authentication); if set to
<code>false</code>, there will be no encryption or authentication. Field
<code>auth_file</code> can be set to an alternative path to the file with
authentication information (see below). Fields <code>cmd_port</code>
(default: 12502) and <code>data_port</code> (default: 12501) can be set
to change the ports of the command channel and the data channel,
respectively.
</p>
<p>If called as <code>pserver (&quot;kill&quot;)</code>, the server will be stopped by
sending it a signal, taking its process id from its pid-file
&rsquo;/tmp/.octave-&lt;hostname&gt;.pid&rsquo;. Otherwise the server will be
started.
</p>
<p>The servers exectuable file (<code>octave-pserver</code>) is searched for
by first assuming the directory structure of a regular package
installation, then by searching Octaves function search path for
it, and then by the called shell in its shell search path.
</p>
<p>If a directory path corresponding to the current directory of the
client exists on the server machine, it will be used as the servers
current directory for the respective client (multiple clients are
possible). Otherwise, <code>/tmp</code> will be used. The Octave
functions the server is supposed to call and the files it possibly
has to access must be available at the server machine. This can
e.g. be achieved by having the server machine mount a network file
system (which is outside the scope of this package documentation).
</p>
<p>If a connection is accepted from a client, the server collects a
network identifier and the names of all server machines of the
network from the client. Then, connections are automatically
established between all machines of the network. Data exchange will
be possible between all machines (client or server) in both
directions. Commands can only be sent from the client to any
server.
</p>
<p>The opaque variable holding the network connections, in the same
order as in the corresponding variable returned by <code>pconnect</code>,
is accessible under the variable name <code>sockets</code> at the server
side. Do not overwrite or clear this variable. The own server
machine will also be contained at some index position of this
variable, but will not correspond to a real connection. See
<code>pconnect</code> for further information.
</p>
<p>The client and the server must both use or both not use TLS. If TLS
is switched off, different measures must be taken to protect ports
of the command and data channels at the servers and the client
against unauthorized access, e.g. by a firewall or by physical
isolation of the network.
</p>
<p>For using TLS, authorization data must be present at the server
machine. These data can conveniently be generated by
<code>parallel_generate_srp_data</code>; the helptext of the latter
function documents the expected location of these data.
</p>
<p>The SRP password will be sent over the encrypted TLS channel from
the client to each server, to avoid permanently storing passwords
at the server for server-to-server data connections. Due to
inevitable usage of external libraries, memory with sensitive data
can, however, be on the swap device even after shutdown of the
application.
</p>
<p>The server executable file <code>octave-pserver</code> is installed and
runs at GNU/Linux, but not at some other operating systems like
Windows and macOS.
</p>

<p><strong>See also:</strong> <a href="pconnect.html#XREFpconnect">pconnect</a>, <a href="reval.html#XREFreval">reval</a>, <a href="psend.html#XREFpsend">psend</a>, <a href="precv.html#XREFprecv">precv</a>, <a href="sclose.html#XREFsclose">sclose</a>, <a href="Authentication.html#XREFparallel_005fgenerate_005fsrp_005fdata">parallel_generate_srp_data</a>, <a href="select_005fsockets.html#XREFselect_005fsockets">select_sockets</a>.
</p></dd></dl>



<hr>
<div class="header">
<p>
Next: <a href="pconnect.html" accesskey="n" rel="next">pconnect</a>, Previous: <a href="Authentication.html" accesskey="p" rel="prev">Authentication</a>, Up: <a href="Cluster-execution.html" accesskey="u" rel="up">Cluster execution</a> &nbsp; [<a href="Function-index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
