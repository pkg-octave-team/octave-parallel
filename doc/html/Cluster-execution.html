<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- General documentation for the parallel package for Octave.

Copyright (C) 2016-2023 Olaf Till <i7tiol@t-online.de>

You can redistribute this documentation and/or modify it under the terms
of the GNU General Public License as published by the Free Software
Foundation; either version 3 of the License, or (at your option) any
later version.

This documentation is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this documentation; if not, see <http://www.gnu.org/licenses/>. -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Cluster execution (parallel_doc)</title>

<meta name="description" content="Cluster execution (parallel_doc)">
<meta name="keywords" content="Cluster execution (parallel_doc)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<link href="index.html" rel="start" title="Top">
<link href="Function-index.html" rel="index" title="Function index">
<link href="index.html" rel="up" title="Top">
<link href="Security.html" rel="next" title="Security">
<link href="parcellfun_005fset_005fnproc.html" rel="prev" title="parcellfun_set_nproc">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Cluster-execution"></span><div class="header">
<p>
Next: <a href="Further-functions.html" accesskey="n" rel="next">Further functions</a>, Previous: <a href="Local-execution.html" accesskey="p" rel="prev">Local execution</a>, Up: <a href="index.html" accesskey="u" rel="up">Top</a> &nbsp; [<a href="Function-index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Functions-for-parallel-execution-over-a-cluster-of-machines"></span><h2 class="chapter">3 Functions for parallel execution over a cluster of machines</h2>
<span id="index-cluster-execution"></span>

<p>There are high level functions, similar to <a href="parcellfun.html">parcellfun</a> and
<a href="pararrayfun.html">pararrayfun</a>, which only require the user to provide a function to
be called and a series of argument sets. Parallel function calls are
then internally distributed over the cluster, and over the local
processors (or processor cores) within the single machines of the
cluster.
</p>
<p>Medium and low level functions are also provided. These require the user
to program the scheduling of parallel execution.
</p>
<p>Data transfer is possible between the client machine and each server
machine, but also directly between server machines. The latter feature
is exploited e.g. by the <a href="install_005fvars.html">install_vars</a> function.
</p>
<p>For limitations, see <a href="Limitations.html">Limitations</a>.
</p>
<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="Security.html" accesskey="1">Security</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Security considerations.
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Authentication.html" accesskey="2">Authentication</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Generating authentication keys.
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="pserver.html" accesskey="3">pserver</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Starting servers.
</td></tr>
<tr><th colspan="3" align="left" valign="top"><pre class="menu-comment">

Connection-related functions
</pre></th></tr><tr><td align="left" valign="top">&bull; <a href="pconnect.html" accesskey="4">pconnect</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Establishing cluster connections.
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="sclose.html" accesskey="5">sclose</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Closing cluster connections.
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="network_005fget_005finfo.html" accesskey="6">network_get_info</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Get network information.
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="network_005fset.html" accesskey="7">network_set</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Set network properties.
</td></tr>
<tr><th colspan="3" align="left" valign="top"><pre class="menu-comment">

High level functions
</pre></th></tr><tr><td align="left" valign="top">&bull; <a href="netcellfun.html" accesskey="8">netcellfun</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Function netcellfun.
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="netarrayfun.html" accesskey="9">netarrayfun</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Function netarrayfun.
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="install_005fvars.html">install_vars</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Distribute Octave variables.
</td></tr>
<tr><th colspan="3" align="left" valign="top"><pre class="menu-comment">

Medium level functions
</pre></th></tr><tr><td align="left" valign="top">&bull; <a href="rfeval.html">rfeval</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Single remote function evaluation.
</td></tr>
<tr><th colspan="3" align="left" valign="top"><pre class="menu-comment">

Low level functions
</pre></th></tr><tr><td align="left" valign="top">&bull; <a href="psend.html">psend</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Sending Octave variables.
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="precv.html">precv</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Receiving Octave variables.
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="reval.html">reval</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Remote command evaluation.
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="select_005fsockets.html">select_sockets</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Call Unix <code>select</code> for data
                                      connections.
</td></tr>
<tr><th colspan="3" align="left" valign="top"><pre class="menu-comment">

</pre></th></tr><tr><td align="left" valign="top">&bull; <a href="Example.html">Example</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Example
</td></tr>
</table>


<hr>
<div class="header">
<p>
Next: <a href="Further-functions.html" accesskey="n" rel="next">Further functions</a>, Previous: <a href="Local-execution.html" accesskey="p" rel="prev">Local execution</a>, Up: <a href="index.html" accesskey="u" rel="up">Top</a> &nbsp; [<a href="Function-index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
